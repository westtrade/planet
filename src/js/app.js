'use strict';

require('es6-promise').polyfill();


var GAME = require('./game.js').init();

GAME.state.add('planet', require('./scenes/planet.js')('default'));
GAME.state.start('planet');