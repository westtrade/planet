'use strict';


var planetBoundingBox;
var FreeCameraXCraftControlInput = require('../xcraft.input.js');


module.exports = function makePlanetScene(planetName) {
	
	var planetInfoUrl = "src/planets/" + planetName + '/';

	return {

		preload : function (cb) {

			var me = this;


			//this.scene.debugLayer.show();

			// var camSensor = new BABYLON.Mesh.CreateBox("sensor", 1, me.scene);
			// camSensor.material = new BABYLON.StandardMaterial("camMat", me.scene);
			// camSensor.isVisible = true;
			// camSensor.material.wireframe = true;
			// camSensor.scaling = new BABYLON.Vector3(.01, .01, .01);
			// camSensor.position = new BABYLON.Vector3(0, .005, 0);
			// camSensor.parent = me.camera;





			// var testMesh = new BABYLON.Mesh.CreateBox("testMesh", 40, me.scene);
			// testMesh.position = BABYLON.Vector3.Zero();
			// //currentPlanetGroup.isVisible = false;

			// currentPlanetGroup = new BABYLON.Mesh.CreateBox("currentPlanet", 1200, me.scene);
			// currentPlanetGroup.position = BABYLON.Vector3.Zero();
			// currentPlanetGroup.isVisible = false;
			//currentPlanetGroup.isPickable = true;

			// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
			// console.log('Collider radius', currentPlanetGroup._collider.radius);


			// console.log('Meshes', this.scene.meshes);








			// var halfSize = 300;

			// currentPlanetGroup.showBoundingBox = true;
			// currentPlanetGroup.checkCollisions = true; 

			//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));
			//console.log(currentPlanetGroup.getBoundingInfo());
			//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));


			//currentPlanetGroup.getScene().collisionCoordinator.onMeshUpdated(currentPlanetGroup);

			//currentPlanetGroup._collider
			
			//console.log();

			// currentPlanetGroup.getScene().collisionCoordinator.getNewPosition(
   //            	currentPlanetGroup._oldPositionForCollisions,
   //            	new BABYLON.Vector3(0, 0, 0),
   //            	currentPlanetGroup._collider,
   //            	3,
   //            	currentPlanetGroup,
   //            	currentPlanetGroup._onCollisionPositionChange,
   //            	currentPlanetGroup.uniqueId
   //        	);

			// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
			// console.log('Collider radius', currentPlanetGroup._collider);


			
			// console.log(currentPlanetGroup.getBoundingInfo());






			// var newPlanetGroup = new BABYLON.Mesh.CreateBox("currentPlanet2", 1350, me.scene);
			// newPlanetGroup.position = BABYLON.Vector3.Zero();

			// console.log('Elsipsoid', newPlanetGroup.ellipsoid);
			// console.log('Collider radius', newPlanetGroup._collider);

			// var halfSize = 300;

			// newPlanetGroup.showBoundingBox = true;
			// newPlanetGroup.checkCollisions = true; 


			// this.scene.meshes.forEach(function (mesh) {
			// 	console.log(mesh.name);
			// })

			//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));
			//console.log(currentPlanetGroup.getBoundingInfo());
			// currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));

			// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
			// console.log('Collider radius', currentPlanetGroup._collider.radius);


			
			// console.log(currentPlanetGroup.getBoundingInfo());







			//currentPlanetGroup.getBoundingInfo().update(currentPlanetGroup.worldMatrixFromCache);
			// currentPlanetGroup.computeWorldMatrix(true);
			// this.camera.computeWorldMatrix(true);
			//currentPlanetGroup.getBoundingInfo().update();

			//currentPlanetGroup.refreshBoundingInfo()
 
			//currentPlanetGroup._boundingInfo.update();



			// currentPlanetGroup.onCollide = function (mesh) {
			// 	//console.log(mesh);
			// 	console.log('```````````````````````````````````````Mesh collide````````````````````````````````````````');

			// }

			// this.camera.onCollide = function () {
			// 	console.log('```````````````````````````````````````Camera collide````````````````````````````````````````');
			// }	


			// var matBB = new BABYLON.StandardMaterial("matBB", me.scene);
			// matBB.emissiveColor = new BABYLON.Color3(1, 1, 1);
			// matBB.wireframe = true;

			// currentPlanetGroup.material = matBB;



			BABYLON.SceneLoader.ImportMesh('',  planetInfoUrl, "object.json", this.scene, function loadSuccess (newMeshes) {

				// console.log('Meshes was loaded');

				planetBoundingBox = new BABYLON.Mesh.CreateSphere("planetBoundingBox", 10, 320, me.scene);
				

				planetBoundingBox.position = BABYLON.Vector3.Zero();
				planetBoundingBox.checkCollisions = true;
				planetBoundingBox.isVisible = false;



				// var vls = new BABYLON.VolumetricLightScatteringPostProcess('vls', { postProcessRatio: 1.0, passRatio: 0.5 }, me.camera, planetBoundingBox, 75, BABYLON.Texture.BILINEAR_SAMPLINGMODE, me.engine, false);



				var wireframeMaterial = new BABYLON.StandardMaterial("wireframe", me.scene);   
				wireframeMaterial.wireframe = true;

				planetBoundingBox.material = wireframeMaterial;

				newMeshes.forEach(function (mesh) {
					mesh.parent = planetBoundingBox;
					me.scene.stopAnimation(mesh);
				});

				planetBoundingBox.position = BABYLON.Vector3.Zero();	

				cb();


			});
			
		},

		create : function () {

			var me = this;
			
			me.scene.clearColor = 'black';
			me.scene.collisionsEnabled = true;


			var skybox = BABYLON.Mesh.CreateBox("skyBox", 5000.0, me.scene);



			var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
			skyboxMaterial.backFaceCulling = false;
			skyboxMaterial.disableLighting = true;
			skybox.infiniteDistance = true;

			
			skybox.material = skyboxMaterial;
			skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
			skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
			skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(planetInfoUrl + "skybox/1", this.scene);
			skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;




			me.camera.inputs.add(new FreeCameraXCraftControlInput());	
			me.camera.inputs.removeByType(me.camera.inputs.attached.keyboard.getTypeName());
			me.camera.attachControl(me.canvas, false);



			// console.log(me.camera.fov);

			// me.camera.fov = 0.1

			me.camera.position = new BABYLON.Vector3(0,  100, -200);
			me.camera.cameraDirection = new BABYLON.Vector3(0, 0, 0);
			me.camera.setTarget(new BABYLON.Vector3(-100, 100, 0));


			// me.camera.cameraRotation = new BABYLON.Vector3(0, -1);


		    
			var planetMesh = me.scene.getMeshByName("planetBoundingBox");

			planetMesh.scaling.x = 1.3;
			planetMesh.scaling.y = 1.3;
			planetMesh.scaling.z = 1.3;


			// planetMesh.scale.y = 2;


		    /* Motion blur
			BABYLON.Effect.ShadersStore["bwPixelShader"] =
			"precision highp float;" +
			"varying vec2 vUV;" +
			"uniform sampler2D textureSampler;" +
			"uniform sampler2D scene0Sampler;" +
			"uniform sampler2D scene1Sampler;" +
			"void main(void)" + 
			"{" +
			"	gl_FragColor = texture2D(textureSampler, vUV) * 0.5 + texture2D(scene0Sampler, vUV) * 0.25 + texture2D(scene1Sampler, vUV) * 0.25;" +
			"}";


			var copies = [];
			copies.push(new BABYLON.PassPostProcess("Scene copy#1", 1.0, me.camera));
			copies.push(new BABYLON.PassPostProcess("Scene copy#2", 1.0, me.camera));
			copies.push(new BABYLON.PassPostProcess("Scene copy#3", 1.0, me.camera));
			var current = 1;
			
			me.camera.detachPostProcess(copies[1]);	
			me.camera.detachPostProcess(copies[2]);

			var postProcess = new BABYLON.PostProcess("BW", "bw", ["time"], ["scene0Sampler", "scene1Sampler"], 1.0, me.camera, BABYLON.Texture.BILINEAR_SAMPLINGMODE); 
			    postProcess.onApply = function (effect) { 
					var next = current + 1;
					
					if (next === copies.length) {
						next = 0;
					}
					effect.setTextureFromPostProcess("scene0Sampler", copies[current]);
					effect.setTextureFromPostProcess("scene1Sampler", copies[next]);						
			    }; 
			*/ 




		    this.scene.registerBeforeRender(function () {

		    	/* Motion blur
		    	var prev = current - 1;
		
				if (prev < 0) {
					prev = copies.length - 1;
				}
				
				me.camera.detachPostProcess(copies[prev]);
				
				current++;
				
				if (current == copies.length) {
					current = 0;
				}
				
				var prev = current - 1;
				
				if (prev < 0) {
					prev = copies.length - 1;
				}
				
				me.camera.attachPostProcess(copies[prev], 0);
				*/






				// me.scene.getMeshByName("planetBoundingBox").rotation.y += 0.05; 

		    	
		    	if (planetMesh) {
		    		planetMesh.rotation.y += .0003; 
		    	}


		    	// var skyBoxMesh = me.scene.getMeshByName("skyBox");
		    	// if (planetMesh) {
		    	// 	skyBoxMesh.rotation.y += .001; 
		    	// }

		    });

		    new BABYLON.HemisphericLight('mainLight', new BABYLON.Vector3(0, 1, 0), me.scene);

		},

		update : function () {

		},

		// render : function () {
			
		// }
	}
};