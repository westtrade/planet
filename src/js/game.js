'use strict';



function createCanvas(canvasId) {
	
	canvasId = canvasId || 'gameCanvas';	
	var canvas = document.createElement('canvas');
	canvas.id     = canvasId;
	document.body.appendChild(canvas);

	return canvas;
}

function init() {

	var canvas = createCanvas('gameCanvas');	
	var gameEngine = new BABYLON.Engine(canvas, true);
	gameEngine.enableOfflineSupport = true;


	//var controlPad = new initControl();
		
	window.addEventListener("resize", function () {
		gameEngine.resize(); // Watch for browser/canvas resize events
	});


	document.body.addEventListener("resize", function () {
		gameEngine.resize(); // Watch for browser/canvas resize events
	});

	var GAME = {
		// controlPad : controlPad,
		scene : null,
		camera : null,
		canvas : canvas,
		engine : gameEngine,

		state : {

			states : { },
			
			add : function (stateName, hooks) {
				
				this.states[stateName] = hooks;
				return this;
			},

			start : function (stateName) {

				var stateExists = stateName in this.states;

				if (!stateExists) {
					//TODO add error
					return this;
				}

				var me = GAME;
				var sceneHooks = this.states[stateName];

				GAME.scene = new BABYLON.Scene(gameEngine, true);
				GAME.scene.collisionsEnabled = true;


				GAME.camera = new BABYLON.FreeCamera('player', new BABYLON.Vector3(0, 0, -700), GAME.scene);

				GAME.scene.activeCamera = GAME.camera;


				// console.log(GAME.camera.inputs.attached.mouse.detach());



				GAME.camera.ellipsoid = new BABYLON.Vector3(10, 10, 10);
		    	GAME.camera.checkCollisions = true;



				// GAME.scene.enablePhysics = true;
				// GAME.camera.inputs.attached.xcraftStick.detachControl();


				var next = function () {

					try {
					
						'create' in sceneHooks && sceneHooks.create.apply(GAME);

						gameEngine.runRenderLoop(function () {
							'update' in sceneHooks && sceneHooks.update.apply(GAME);
							'render' in sceneHooks ? sceneHooks.render.apply(GAME) : me.scene.render();						
						});

					} catch (e) {
						console.log(e.stack);
					}
				}


				try {
					'preload' in sceneHooks ? sceneHooks.preload.apply(GAME, [next]) : next();
				} catch (e) {
					console.log(e.stack);
				}
				


				return this;
			}
		}
	}

	return GAME;
}



module.exports['createCanvas'] = createCanvas;
module.exports['init'] = init;


