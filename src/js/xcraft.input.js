'use strict';


function initControl(options) {

    var controlPad = document.createElement('div');
    controlPad.id = 'control';
    document.body.appendChild(controlPad);

    var padHandleStyle = null;
    var controlPadSize = 154;
    var padHandleSize = 86;
    var controlActive = false;
    var mouseGlobalX, mouseGLobalY;


    var defaultOptions = {
        keys : {
            up : 87,
            left : 65,
            down : 83,
            right : 68
        }
    }

    options = options || defaultOptions; //TODO add options merge


    var keyboardKeys = {
        up : [38],
        down : [40],
        left : [37],
        right : [39]
    }


    if (options instanceof Object && 'keys' in options && options.keys instanceof Object) {

        Object.keys(keyboardKeys).forEach(function (keyIdx) {
            
            var keyCodeExists = keyIdx in options.keys;

            if (!keyCodeExists) {
                return;
            }

            var currentKeysFromOptions = options.keys[keyIdx];
            currentKeysFromOptions = typeof currentKeysFromOptions === 'string'   ? parseInt(currentKeysFromOptions) : currentKeysFromOptions;
            var isAllowedArray = currentKeysFromOptions instanceof Array, isAllowedNUmber = typeof currentKeysFromOptions === 'number';

            if (!isAllowedArray) {
                currentKeysFromOptions = [currentKeysFromOptions];
            }

            currentKeysFromOptions
                .forEach(function (keyCode) {
                    keyCode = parseInt(keyCode);
                    if (!~keyboardKeys[keyIdx].indexOf(keyCode)) keyboardKeys[keyIdx].push(keyCode);                    
                });
            ;      

        });
    } 

    // 154 x 154 - field
    // 86 x 86 - pad


    function getElementPosition(element) {
        var boundRectOfPad = element.getBoundingClientRect();
        return { x: boundRectOfPad.left + window.scrollX, y: boundRectOfPad.top + window.scrollY }
    }

    
    function convertPositionToPadRelated(controlPadRelatedPosition) {
        
        var resultPosition = {
            x : 0, y : 0
        };

        resultPosition.x = controlPadRelatedPosition.x > 0 ? 0 : Math.abs(controlPadRelatedPosition.x);
        resultPosition.y = controlPadRelatedPosition.y > 0 ? 0 : Math.abs(controlPadRelatedPosition.y);

        resultPosition.x = resultPosition.x > controlPadSize ? controlPadSize : resultPosition.x;
        resultPosition.y = resultPosition.y > controlPadSize ? controlPadSize : resultPosition.y;

        return resultPosition;
    }

    
    function getOffsetRect(offset) {    
        
        return {            
            maxX : offset / 2 + padHandleSize,
            maxY : offset / 2 + padHandleSize,
            minX : offset,
            minY : offset,
        }
    }

    var leftRight = 0;
    var topBottom = 0;

    function getPadHandleCoords(relatedOnControlPosition) {
        
        var offset = padHandleSize / 2, resultCoords = { x: 0, y : 0 };

        resultCoords.x = relatedOnControlPosition.x - offset;
        resultCoords.y = relatedOnControlPosition.y - offset;

        var offsetRect = getOffsetRect(16);
        
        resultCoords.x = resultCoords.x < offsetRect.minX ? offsetRect.minX : resultCoords.x;
        resultCoords.x = resultCoords.x > offsetRect.maxX ? offsetRect.maxX : resultCoords.x;

        resultCoords.y = resultCoords.y < offsetRect.minY ? offsetRect.minY : resultCoords.y;
        resultCoords.y = resultCoords.y > offsetRect.maxY ? offsetRect.maxY : resultCoords.y;

        resultCoords.x = resultCoords.x > controlPadSize ? controlPadSize : resultCoords.x;
        resultCoords.y = resultCoords.y > controlPadSize ? controlPadSize : resultCoords.y;

        var rectHalf = 78 / 2, rectHalfMin = rectHalf - 10, rectHalfMax = rectHalf + 10;

        var correctX = resultCoords.x - 16;

        if (correctX < rectHalfMin) {
            leftRight = -1;
        } else if (correctX > rectHalfMax) {
            leftRight = 1;
        }

        var correctY = resultCoords.y - 16;

        if (correctY < rectHalfMin) {
            topBottom = 1;
        } else if (correctY > rectHalfMax) {
            topBottom = -1;
        }



        return resultCoords;
    }


    function setPadHandlePosition(mouseX, mouseY) {     
        
        var position = getElementPosition(controlPad);
        var relatedPosition = {
            x : position.x - mouseX,
            y : position.y - mouseY,
        }

        var convertedPosition = convertPositionToPadRelated(relatedPosition);
        controlPad.movePadTo(convertedPosition);
    }   



    controlPad.movePadTo = function (handleCoordsOnPad) {
        
        if (!padHandleStyle) {

            var styleELement = document.createElement('style');
            document.head.appendChild(styleELement);

            var styleSheet = styleELement.sheet;
            var str = ''
            styleSheet.insertRule('#control::before{ ' + str + ' }', styleSheet.cssRules.length);
            
            padHandleStyle = styleSheet.cssRules[styleSheet.cssRules.length - 1];
        }

        var padHandleCoords = getPadHandleCoords(handleCoordsOnPad);
        padHandleStyle.style.backgroundPosition = padHandleCoords.x + 'px ' + padHandleCoords.y + 'px';
    }



    controlPad.movePadToCenter = function () {
        
        if (!padHandleStyle) {

            var styleELement = document.createElement('style');
            document.head.appendChild(styleELement);

            var styleSheet = styleELement.sheet;
            var str = ''
            styleSheet.insertRule('#control::before{ ' + str + ' }', styleSheet.cssRules.length);
            
            padHandleStyle = styleSheet.cssRules[styleSheet.cssRules.length - 1];
        }

        padHandleStyle.style.backgroundPosition = 'center center';
    }



    var intervalChecker = {
        
        checkIntervalID : null,
        
        start : function (onTimerCallback) {
            
            var me  = this;

            if (me.checkIntervalID) {
                this.stop();
            } 

            onTimerCallback();          

            me.checkIntervalID = setInterval(function () {
                if (onTimerCallback) {
                    onTimerCallback();
                }
            }, 10);
        },

        stop : function () {

            var me  = this;            
            if (!me.checkIntervalID) {
                return ;
            }

            clearInterval(me.checkIntervalID)
        }
    }


    

    var moveMatrix = {

        stop : function () {
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }

    }, _moveMatrix = {
        x : 0, y : 0, z: 0,
        isStoped : function () {
            return this.x == 0 && this.y == 0 && this.z ==0;
        },
        startMove : function () {
            
            if (this.isStoped()) {
                var PadEvent = new CustomEvent('actorstartmove', {  });
                controlPad.dispatchEvent(PadEvent);
            }
        },

        stopMove : function () {
            
            if (this.isStoped()) {
                var PadEvent = new CustomEvent('actorstopmove', {  });
                controlPad.dispatchEvent(PadEvent);
            }

        },

        moveTo : function (axis, velocity) {
            this.startMove();            
            this[axis] = velocity;
            this.stopMove();        
        }
    };


    Object.defineProperty(moveMatrix, 'x', {
        configurable: false,
        enumerable: true,        
        get : function () {
            return _moveMatrix['x'];
        },        
        set : function (value) {
            _moveMatrix.moveTo('x', parseInt(value));
        }
    });

    Object.defineProperty(moveMatrix, 'isStoped', {
        configurable: false,
        enumerable: true,        
        get : function () {
            return _moveMatrix.isStoped();
        }        
        // set : function (value) {
        //     _moveMatrix.moveTo('x', parseInt(value));
        // }
    });


    Object.defineProperty(moveMatrix, 'y', {
        configurable: false,
        enumerable: true,        
        get : function () {
            return _moveMatrix['y'];
        },        
        set : function (value) {
            _moveMatrix.moveTo('y', parseInt(value));
        }
    });

    
    Object.defineProperty(moveMatrix, 'z', {
        configurable: false,
        enumerable: true,        
        get : function () {
            return _moveMatrix['z'];
        },        
        set : function (value) {
            _moveMatrix.moveTo('z', parseInt(value));
        }
    });


    intervalChecker.start(function () {
        var PadEvent = new CustomEvent('actormove', { detail : moveMatrix });
        controlPad.dispatchEvent(PadEvent);
    })



    var keyWatcher = {
        keys : [],
       
        press : function (keyName) {
            if (this.has(keyName)) return;
            this.keys.push(keyName);
        },

        has : function (keyName) {
            return ~this.keys.indexOf(keyName);
        },

        unpress : function (keyName) {
            if (!this.has(keyName)) return;
            this.keys.splice(this.keys.indexOf(keyName), 1);            
        }
    }

    
    var behaviors = {
        
        start : function (event) {
            controlActive = true;
            moveMatrix.z = 1;
        },

        move : function (event) {
            

            if (controlActive) {
                setPadHandlePosition(event.pageX || e.originalEvent.touches[0].pageX, event.pageY || e.originalEvent.touches[0].pageY);
                moveMatrix.x = 1 * leftRight;
                moveMatrix.y = 1 * topBottom;
                moveMatrix.z = 1;
            }
        },

        stop : function (event) {
            
            controlActive = false;
            controlPad.movePadToCenter();

            moveMatrix.stop();

            // var PadEvent = new CustomEvent('actorstopmove', { });
            // controlPad.dispatchEvent(PadEvent);

            // intervalChecker.stop();
        },

        keyboardDispatcher : function (event) {

            if (~keyboardKeys.up.indexOf(event.keyCode)) {

                if (event.type == 'keydown') {  
                    keyWatcher.press('forward');                    
                } 

                if (event.type == 'keyup') {
                    keyWatcher.unpress('forward');
                }
            }

            else if (~keyboardKeys.left.indexOf(event.keyCode)) {

                if (event.type == 'keydown') {
                    keyWatcher.press('left');                    
                }

                if (event.type == 'keyup') {
                    keyWatcher.unpress('left');
                }
            }

            else if (~keyboardKeys.right.indexOf(event.keyCode)) {
                
                if (event.type == 'keydown') {
                    keyWatcher.press('right');                    
                }

                if (event.type == 'keyup') {
                    keyWatcher.unpress('right');
                }
            }

            else if (~keyboardKeys.down.indexOf(event.keyCode)) {
                
                if (event.type == 'keydown') {
                    keyWatcher.press('backward');                    
                }

                if (event.type == 'keyup') {
                    keyWatcher.unpress('backward');
                }
            }   else {
                return ;
            }



            if (!keyWatcher.keys.length) {

                console.log();
                return moveMatrix.stop();
            }

            var isSideMove = keyWatcher.has('left') || keyWatcher.has('right');
            var isDirectMove = keyWatcher.has('forward') || keyWatcher.has('backward');

            if (isDirectMove && !isSideMove) {
                // console.log('isDIrect move');
                moveMatrix.z = keyWatcher.has('forward') && keyWatcher.has('backward')  ? 0 :
                    keyWatcher.has('forward') ?  1 : -1;

                moveMatrix.x = 0;

                return ;
            }   

            if (isSideMove) {

                moveMatrix.x = keyWatcher.has('left') && keyWatcher.has('right')  ? 0 :
                    keyWatcher.has('left') ?  -1 : 1;

                var moveIsForward = keyWatcher.has('forward') || 
                    !keyWatcher.has('backward') ||
                    (keyWatcher.has('forward') && keyWatcher.has('backward'));                

                moveMatrix.z =  moveIsForward ? 1 : -1;

            } 
            
            //return moveMatrix.stop();            

        }
    }

    controlPad.addEventListener('mousedown', behaviors.start, false);   
    controlPad.addEventListener('touchstart', behaviors.start, false);
    document.addEventListener('mousemove', behaviors.move, false);
    document.addEventListener('touchmove', behaviors.move, false);  
    document.addEventListener('mouseup', behaviors.stop, false);
    document.addEventListener('touchend', behaviors.stop, false);       
   
    document.addEventListener('keydown', behaviors.keyboardDispatcher, false);
    document.addEventListener('keyup', behaviors.keyboardDispatcher, false);  
    document.addEventListener('blur', behaviors.keyboardDispatcher, false);


    return {
        pad : controlPad,
        detach : function () {    

            controlPad.removeEventListener('mousedown', behaviors.start, false);   
            controlPad.removeEventListener('touchstart', behaviors.start, false);
            document.removeEventListener('mousemove', behaviors.move, false);
            document.removeEventListener('touchmove', behaviors.move, false);  
            document.removeEventListener('mouseup', behaviors.stop, false);
            document.removeEventListener('touchend', behaviors.stop, false);              

            document.removeEventListener('keydown', behaviors.keyboardDispatcher, false);
            document.removeEventListener('keyup', behaviors.keyboardDispatcher, false);  
            document.removeEventListener('blur', behaviors.keyboardDispatcher, false);

            controlPad.parentNode.removeChild(controlPad);
            intervalChecker.stop();

        }
    };
}

function FreeCameraXCraftControlInput(stickSpeed, angularSensibility) {

    this.angularSensibility = angularSensibility || 1000;
    this._stickSpeed = stickSpeed || 0.7;
    this._controlPad = null;
    this._isMoved = false;
    // this._moves = [];

    this._currentMove = null;
};

FreeCameraXCraftControlInput.prototype.detachControl = function (element) {

    if (this._controlPad) {
        this._controlPad.detach();
        delete this._controlPad;
        this._controlPad = null;
    }
};


FreeCameraXCraftControlInput.prototype.attachControl = function (element, noPreventDefault) {

    var me = this;

    this._controlPad = initControl();
    this._controlPad.pad.addEventListener('actorstartmove', function (event) {

    }, false);

    this._controlPad.pad.addEventListener('actorstopmove', function (event) {        
        me._currentMove = null;
    }, false);

    this._controlPad.pad.addEventListener('actormove', function (event) {
        me._currentMove = event.detail;
    }, false);

};


FreeCameraXCraftControlInput.prototype.getTypeName = function (element) {
    return "FreeCameraXCraftControlInput";
};


FreeCameraXCraftControlInput.prototype.getSimpleName = function (element) {
    return "xcraftStick";
};

FreeCameraXCraftControlInput.prototype.checkInputs = function () {

    var camera = this.camera;
    if (this._currentMove && !this._currentMove.isStoped) {

        var speed = camera._computeLocalCameraSpeed();
        camera._localDirection.copyFromFloats(
            this._stickSpeed * speed * this._currentMove.x,
            0, // this._stickSpeed *  speed * this._currentMove.y,
            this._stickSpeed *  speed * this._currentMove.z
        );

        camera.getViewMatrix().invertToRef(camera._cameraTransformMatrix);

        //this.angularSensibility = 100;

        if (this._currentMove.y !== 0) {
            camera.cameraRotation.x -= this._currentMove.y / this.angularSensibility;
        }

        if (this._currentMove.x !== 0) {
            camera.cameraRotation.y += this._currentMove.x / this.angularSensibility;
        }

        BABYLON.Vector3.TransformNormalToRef(camera._localDirection, camera._cameraTransformMatrix, camera._transformedDirection);
        camera.cameraDirection.addInPlace(camera._transformedDirection);
    }

}

module.exports = FreeCameraXCraftControlInput;


