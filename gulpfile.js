'use strict';

let path = require('path');
let gulp = require('gulp');
let gutil = require('gulp-util');
let watch = require('gulp-watch');
let NpmImportPlugin = require("less-plugin-npm-import");
let less = require('gulp-less');
let plumber = require('gulp-plumber');

let lessPath = './src/less/**/*.less';
let jsPath = './src/js/**/*.js';


const plumberOptions = {
	errorHandler : function defaultErrorHandler(error) {
		
		this.emit('end');

		// onerror2 and this handler
		if (EE.listenerCount(this, 'error') < 3) {
			gutil.log(
				gutil.colors.cyan('Plumber') + gutil.colors.red(' found unhandled error:\n'),
				error.toString()
			);
		}

	}
}

gulp.task('watch', () => { 
	gulp.watch('./src/less/**', ['less']) 
	//gulp.watch('./src/js/**', ['scripts']) 
});

gulp.task('less', () => {
	
	let settings = {
		paths: [path.join(__dirname, 'less', 'includes')],
		plugins: [new NpmImportPlugin()]
	};

	let lessComposer = less(settings);

	return gulp
		.src(lessPath)
		.pipe(plumber(plumberOptions))
		.pipe(lessComposer)
	    .pipe(plumber.stop())
		.pipe(gulp.dest('./build/css'))
		
		;
}); 




const concat = require('gulp-concat');
const gulpWebpack = require('gulp-webpack');

const BomPlugin = require('webpack-utf8-bom');


const webPackSettings = {
	watch : false,
	output: {
		filename: 'app.js',
        comments: false

	},
	plugins: [
		//new BomPlugin(true)
	],
};

gulp.task('scripts', () => {
	
	let scriptsCompiler = gulpWebpack(webPackSettings);

	return gulp
		.src('./src/js/app.js')
		.pipe(plumber(plumberOptions))
		.pipe(scriptsCompiler)
	    .pipe(plumber.stop())
		.pipe(gulp.dest('./build/js'))
		
		;
});



// gulp.task('lessStream', () => {
	
//     watch(lessPath, function () {

// 		let settings = {
// 			paths: [path.join(__dirname, 'less', 'includes')],
// 			plugins: [new NpmImportPlugin()]
// 		};

// 		let lessComposer = less(settings);

// 		return gulp
// 			.src(lessPath)
// 			.pipe(watch(lessPath))		
// 			.pipe(plumber(plumberOptions))
// 			.pipe(lessComposer)
// 		    .pipe(plumber.stop())
// 			.pipe(gulp.dest('./build/css'))
// 			//.on('end', cb)

// 			;
// 	});

// }); 


gulp.task('scriptsStream', () => {
	
    watch(jsPath, function () {

		let scriptsCompiler = gulpWebpack(webPackSettings);

		return gulp
			.src('./src/js/app.js')
			.pipe(plumber(plumberOptions))
			.pipe(scriptsCompiler)
		    .pipe(plumber.stop())
			.pipe(gulp.dest('./build/js'))
			
			;
	});
});




gulp.task('default', ['watch', 'scripts',  'less', 'scriptsStream']);
gulp.task('build', ['less', 'scripts'])