/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1).polyfill();


	var GAME = __webpack_require__(6).init();

	GAME.state.add('planet', __webpack_require__(7)('default'));
	GAME.state.start('planet');

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var require;var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(process, global, module) {/*!
	 * @overview es6-promise - a tiny implementation of Promises/A+.
	 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
	 * @license   Licensed under MIT license
	 *            See https://raw.githubusercontent.com/jakearchibald/es6-promise/master/LICENSE
	 * @version   3.2.1
	 */

	(function() {
	    "use strict";
	    function lib$es6$promise$utils$$objectOrFunction(x) {
	      return typeof x === 'function' || (typeof x === 'object' && x !== null);
	    }

	    function lib$es6$promise$utils$$isFunction(x) {
	      return typeof x === 'function';
	    }

	    function lib$es6$promise$utils$$isMaybeThenable(x) {
	      return typeof x === 'object' && x !== null;
	    }

	    var lib$es6$promise$utils$$_isArray;
	    if (!Array.isArray) {
	      lib$es6$promise$utils$$_isArray = function (x) {
	        return Object.prototype.toString.call(x) === '[object Array]';
	      };
	    } else {
	      lib$es6$promise$utils$$_isArray = Array.isArray;
	    }

	    var lib$es6$promise$utils$$isArray = lib$es6$promise$utils$$_isArray;
	    var lib$es6$promise$asap$$len = 0;
	    var lib$es6$promise$asap$$vertxNext;
	    var lib$es6$promise$asap$$customSchedulerFn;

	    var lib$es6$promise$asap$$asap = function asap(callback, arg) {
	      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len] = callback;
	      lib$es6$promise$asap$$queue[lib$es6$promise$asap$$len + 1] = arg;
	      lib$es6$promise$asap$$len += 2;
	      if (lib$es6$promise$asap$$len === 2) {
	        // If len is 2, that means that we need to schedule an async flush.
	        // If additional callbacks are queued before the queue is flushed, they
	        // will be processed by this flush that we are scheduling.
	        if (lib$es6$promise$asap$$customSchedulerFn) {
	          lib$es6$promise$asap$$customSchedulerFn(lib$es6$promise$asap$$flush);
	        } else {
	          lib$es6$promise$asap$$scheduleFlush();
	        }
	      }
	    }

	    function lib$es6$promise$asap$$setScheduler(scheduleFn) {
	      lib$es6$promise$asap$$customSchedulerFn = scheduleFn;
	    }

	    function lib$es6$promise$asap$$setAsap(asapFn) {
	      lib$es6$promise$asap$$asap = asapFn;
	    }

	    var lib$es6$promise$asap$$browserWindow = (typeof window !== 'undefined') ? window : undefined;
	    var lib$es6$promise$asap$$browserGlobal = lib$es6$promise$asap$$browserWindow || {};
	    var lib$es6$promise$asap$$BrowserMutationObserver = lib$es6$promise$asap$$browserGlobal.MutationObserver || lib$es6$promise$asap$$browserGlobal.WebKitMutationObserver;
	    var lib$es6$promise$asap$$isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

	    // test for web worker but not in IE10
	    var lib$es6$promise$asap$$isWorker = typeof Uint8ClampedArray !== 'undefined' &&
	      typeof importScripts !== 'undefined' &&
	      typeof MessageChannel !== 'undefined';

	    // node
	    function lib$es6$promise$asap$$useNextTick() {
	      // node version 0.10.x displays a deprecation warning when nextTick is used recursively
	      // see https://github.com/cujojs/when/issues/410 for details
	      return function() {
	        process.nextTick(lib$es6$promise$asap$$flush);
	      };
	    }

	    // vertx
	    function lib$es6$promise$asap$$useVertxTimer() {
	      return function() {
	        lib$es6$promise$asap$$vertxNext(lib$es6$promise$asap$$flush);
	      };
	    }

	    function lib$es6$promise$asap$$useMutationObserver() {
	      var iterations = 0;
	      var observer = new lib$es6$promise$asap$$BrowserMutationObserver(lib$es6$promise$asap$$flush);
	      var node = document.createTextNode('');
	      observer.observe(node, { characterData: true });

	      return function() {
	        node.data = (iterations = ++iterations % 2);
	      };
	    }

	    // web worker
	    function lib$es6$promise$asap$$useMessageChannel() {
	      var channel = new MessageChannel();
	      channel.port1.onmessage = lib$es6$promise$asap$$flush;
	      return function () {
	        channel.port2.postMessage(0);
	      };
	    }

	    function lib$es6$promise$asap$$useSetTimeout() {
	      return function() {
	        setTimeout(lib$es6$promise$asap$$flush, 1);
	      };
	    }

	    var lib$es6$promise$asap$$queue = new Array(1000);
	    function lib$es6$promise$asap$$flush() {
	      for (var i = 0; i < lib$es6$promise$asap$$len; i+=2) {
	        var callback = lib$es6$promise$asap$$queue[i];
	        var arg = lib$es6$promise$asap$$queue[i+1];

	        callback(arg);

	        lib$es6$promise$asap$$queue[i] = undefined;
	        lib$es6$promise$asap$$queue[i+1] = undefined;
	      }

	      lib$es6$promise$asap$$len = 0;
	    }

	    function lib$es6$promise$asap$$attemptVertx() {
	      try {
	        var r = require;
	        var vertx = __webpack_require__(4);
	        lib$es6$promise$asap$$vertxNext = vertx.runOnLoop || vertx.runOnContext;
	        return lib$es6$promise$asap$$useVertxTimer();
	      } catch(e) {
	        return lib$es6$promise$asap$$useSetTimeout();
	      }
	    }

	    var lib$es6$promise$asap$$scheduleFlush;
	    // Decide what async method to use to triggering processing of queued callbacks:
	    if (lib$es6$promise$asap$$isNode) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useNextTick();
	    } else if (lib$es6$promise$asap$$BrowserMutationObserver) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMutationObserver();
	    } else if (lib$es6$promise$asap$$isWorker) {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useMessageChannel();
	    } else if (lib$es6$promise$asap$$browserWindow === undefined && "function" === 'function') {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$attemptVertx();
	    } else {
	      lib$es6$promise$asap$$scheduleFlush = lib$es6$promise$asap$$useSetTimeout();
	    }
	    function lib$es6$promise$then$$then(onFulfillment, onRejection) {
	      var parent = this;

	      var child = new this.constructor(lib$es6$promise$$internal$$noop);

	      if (child[lib$es6$promise$$internal$$PROMISE_ID] === undefined) {
	        lib$es6$promise$$internal$$makePromise(child);
	      }

	      var state = parent._state;

	      if (state) {
	        var callback = arguments[state - 1];
	        lib$es6$promise$asap$$asap(function(){
	          lib$es6$promise$$internal$$invokeCallback(state, child, callback, parent._result);
	        });
	      } else {
	        lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection);
	      }

	      return child;
	    }
	    var lib$es6$promise$then$$default = lib$es6$promise$then$$then;
	    function lib$es6$promise$promise$resolve$$resolve(object) {
	      /*jshint validthis:true */
	      var Constructor = this;

	      if (object && typeof object === 'object' && object.constructor === Constructor) {
	        return object;
	      }

	      var promise = new Constructor(lib$es6$promise$$internal$$noop);
	      lib$es6$promise$$internal$$resolve(promise, object);
	      return promise;
	    }
	    var lib$es6$promise$promise$resolve$$default = lib$es6$promise$promise$resolve$$resolve;
	    var lib$es6$promise$$internal$$PROMISE_ID = Math.random().toString(36).substring(16);

	    function lib$es6$promise$$internal$$noop() {}

	    var lib$es6$promise$$internal$$PENDING   = void 0;
	    var lib$es6$promise$$internal$$FULFILLED = 1;
	    var lib$es6$promise$$internal$$REJECTED  = 2;

	    var lib$es6$promise$$internal$$GET_THEN_ERROR = new lib$es6$promise$$internal$$ErrorObject();

	    function lib$es6$promise$$internal$$selfFulfillment() {
	      return new TypeError("You cannot resolve a promise with itself");
	    }

	    function lib$es6$promise$$internal$$cannotReturnOwn() {
	      return new TypeError('A promises callback cannot return that same promise.');
	    }

	    function lib$es6$promise$$internal$$getThen(promise) {
	      try {
	        return promise.then;
	      } catch(error) {
	        lib$es6$promise$$internal$$GET_THEN_ERROR.error = error;
	        return lib$es6$promise$$internal$$GET_THEN_ERROR;
	      }
	    }

	    function lib$es6$promise$$internal$$tryThen(then, value, fulfillmentHandler, rejectionHandler) {
	      try {
	        then.call(value, fulfillmentHandler, rejectionHandler);
	      } catch(e) {
	        return e;
	      }
	    }

	    function lib$es6$promise$$internal$$handleForeignThenable(promise, thenable, then) {
	       lib$es6$promise$asap$$asap(function(promise) {
	        var sealed = false;
	        var error = lib$es6$promise$$internal$$tryThen(then, thenable, function(value) {
	          if (sealed) { return; }
	          sealed = true;
	          if (thenable !== value) {
	            lib$es6$promise$$internal$$resolve(promise, value);
	          } else {
	            lib$es6$promise$$internal$$fulfill(promise, value);
	          }
	        }, function(reason) {
	          if (sealed) { return; }
	          sealed = true;

	          lib$es6$promise$$internal$$reject(promise, reason);
	        }, 'Settle: ' + (promise._label || ' unknown promise'));

	        if (!sealed && error) {
	          sealed = true;
	          lib$es6$promise$$internal$$reject(promise, error);
	        }
	      }, promise);
	    }

	    function lib$es6$promise$$internal$$handleOwnThenable(promise, thenable) {
	      if (thenable._state === lib$es6$promise$$internal$$FULFILLED) {
	        lib$es6$promise$$internal$$fulfill(promise, thenable._result);
	      } else if (thenable._state === lib$es6$promise$$internal$$REJECTED) {
	        lib$es6$promise$$internal$$reject(promise, thenable._result);
	      } else {
	        lib$es6$promise$$internal$$subscribe(thenable, undefined, function(value) {
	          lib$es6$promise$$internal$$resolve(promise, value);
	        }, function(reason) {
	          lib$es6$promise$$internal$$reject(promise, reason);
	        });
	      }
	    }

	    function lib$es6$promise$$internal$$handleMaybeThenable(promise, maybeThenable, then) {
	      if (maybeThenable.constructor === promise.constructor &&
	          then === lib$es6$promise$then$$default &&
	          constructor.resolve === lib$es6$promise$promise$resolve$$default) {
	        lib$es6$promise$$internal$$handleOwnThenable(promise, maybeThenable);
	      } else {
	        if (then === lib$es6$promise$$internal$$GET_THEN_ERROR) {
	          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$GET_THEN_ERROR.error);
	        } else if (then === undefined) {
	          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
	        } else if (lib$es6$promise$utils$$isFunction(then)) {
	          lib$es6$promise$$internal$$handleForeignThenable(promise, maybeThenable, then);
	        } else {
	          lib$es6$promise$$internal$$fulfill(promise, maybeThenable);
	        }
	      }
	    }

	    function lib$es6$promise$$internal$$resolve(promise, value) {
	      if (promise === value) {
	        lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$selfFulfillment());
	      } else if (lib$es6$promise$utils$$objectOrFunction(value)) {
	        lib$es6$promise$$internal$$handleMaybeThenable(promise, value, lib$es6$promise$$internal$$getThen(value));
	      } else {
	        lib$es6$promise$$internal$$fulfill(promise, value);
	      }
	    }

	    function lib$es6$promise$$internal$$publishRejection(promise) {
	      if (promise._onerror) {
	        promise._onerror(promise._result);
	      }

	      lib$es6$promise$$internal$$publish(promise);
	    }

	    function lib$es6$promise$$internal$$fulfill(promise, value) {
	      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }

	      promise._result = value;
	      promise._state = lib$es6$promise$$internal$$FULFILLED;

	      if (promise._subscribers.length !== 0) {
	        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, promise);
	      }
	    }

	    function lib$es6$promise$$internal$$reject(promise, reason) {
	      if (promise._state !== lib$es6$promise$$internal$$PENDING) { return; }
	      promise._state = lib$es6$promise$$internal$$REJECTED;
	      promise._result = reason;

	      lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publishRejection, promise);
	    }

	    function lib$es6$promise$$internal$$subscribe(parent, child, onFulfillment, onRejection) {
	      var subscribers = parent._subscribers;
	      var length = subscribers.length;

	      parent._onerror = null;

	      subscribers[length] = child;
	      subscribers[length + lib$es6$promise$$internal$$FULFILLED] = onFulfillment;
	      subscribers[length + lib$es6$promise$$internal$$REJECTED]  = onRejection;

	      if (length === 0 && parent._state) {
	        lib$es6$promise$asap$$asap(lib$es6$promise$$internal$$publish, parent);
	      }
	    }

	    function lib$es6$promise$$internal$$publish(promise) {
	      var subscribers = promise._subscribers;
	      var settled = promise._state;

	      if (subscribers.length === 0) { return; }

	      var child, callback, detail = promise._result;

	      for (var i = 0; i < subscribers.length; i += 3) {
	        child = subscribers[i];
	        callback = subscribers[i + settled];

	        if (child) {
	          lib$es6$promise$$internal$$invokeCallback(settled, child, callback, detail);
	        } else {
	          callback(detail);
	        }
	      }

	      promise._subscribers.length = 0;
	    }

	    function lib$es6$promise$$internal$$ErrorObject() {
	      this.error = null;
	    }

	    var lib$es6$promise$$internal$$TRY_CATCH_ERROR = new lib$es6$promise$$internal$$ErrorObject();

	    function lib$es6$promise$$internal$$tryCatch(callback, detail) {
	      try {
	        return callback(detail);
	      } catch(e) {
	        lib$es6$promise$$internal$$TRY_CATCH_ERROR.error = e;
	        return lib$es6$promise$$internal$$TRY_CATCH_ERROR;
	      }
	    }

	    function lib$es6$promise$$internal$$invokeCallback(settled, promise, callback, detail) {
	      var hasCallback = lib$es6$promise$utils$$isFunction(callback),
	          value, error, succeeded, failed;

	      if (hasCallback) {
	        value = lib$es6$promise$$internal$$tryCatch(callback, detail);

	        if (value === lib$es6$promise$$internal$$TRY_CATCH_ERROR) {
	          failed = true;
	          error = value.error;
	          value = null;
	        } else {
	          succeeded = true;
	        }

	        if (promise === value) {
	          lib$es6$promise$$internal$$reject(promise, lib$es6$promise$$internal$$cannotReturnOwn());
	          return;
	        }

	      } else {
	        value = detail;
	        succeeded = true;
	      }

	      if (promise._state !== lib$es6$promise$$internal$$PENDING) {
	        // noop
	      } else if (hasCallback && succeeded) {
	        lib$es6$promise$$internal$$resolve(promise, value);
	      } else if (failed) {
	        lib$es6$promise$$internal$$reject(promise, error);
	      } else if (settled === lib$es6$promise$$internal$$FULFILLED) {
	        lib$es6$promise$$internal$$fulfill(promise, value);
	      } else if (settled === lib$es6$promise$$internal$$REJECTED) {
	        lib$es6$promise$$internal$$reject(promise, value);
	      }
	    }

	    function lib$es6$promise$$internal$$initializePromise(promise, resolver) {
	      try {
	        resolver(function resolvePromise(value){
	          lib$es6$promise$$internal$$resolve(promise, value);
	        }, function rejectPromise(reason) {
	          lib$es6$promise$$internal$$reject(promise, reason);
	        });
	      } catch(e) {
	        lib$es6$promise$$internal$$reject(promise, e);
	      }
	    }

	    var lib$es6$promise$$internal$$id = 0;
	    function lib$es6$promise$$internal$$nextId() {
	      return lib$es6$promise$$internal$$id++;
	    }

	    function lib$es6$promise$$internal$$makePromise(promise) {
	      promise[lib$es6$promise$$internal$$PROMISE_ID] = lib$es6$promise$$internal$$id++;
	      promise._state = undefined;
	      promise._result = undefined;
	      promise._subscribers = [];
	    }

	    function lib$es6$promise$promise$all$$all(entries) {
	      return new lib$es6$promise$enumerator$$default(this, entries).promise;
	    }
	    var lib$es6$promise$promise$all$$default = lib$es6$promise$promise$all$$all;
	    function lib$es6$promise$promise$race$$race(entries) {
	      /*jshint validthis:true */
	      var Constructor = this;

	      if (!lib$es6$promise$utils$$isArray(entries)) {
	        return new Constructor(function(resolve, reject) {
	          reject(new TypeError('You must pass an array to race.'));
	        });
	      } else {
	        return new Constructor(function(resolve, reject) {
	          var length = entries.length;
	          for (var i = 0; i < length; i++) {
	            Constructor.resolve(entries[i]).then(resolve, reject);
	          }
	        });
	      }
	    }
	    var lib$es6$promise$promise$race$$default = lib$es6$promise$promise$race$$race;
	    function lib$es6$promise$promise$reject$$reject(reason) {
	      /*jshint validthis:true */
	      var Constructor = this;
	      var promise = new Constructor(lib$es6$promise$$internal$$noop);
	      lib$es6$promise$$internal$$reject(promise, reason);
	      return promise;
	    }
	    var lib$es6$promise$promise$reject$$default = lib$es6$promise$promise$reject$$reject;


	    function lib$es6$promise$promise$$needsResolver() {
	      throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
	    }

	    function lib$es6$promise$promise$$needsNew() {
	      throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
	    }

	    var lib$es6$promise$promise$$default = lib$es6$promise$promise$$Promise;
	    /**
	      Promise objects represent the eventual result of an asynchronous operation. The
	      primary way of interacting with a promise is through its `then` method, which
	      registers callbacks to receive either a promise's eventual value or the reason
	      why the promise cannot be fulfilled.

	      Terminology
	      -----------

	      - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
	      - `thenable` is an object or function that defines a `then` method.
	      - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
	      - `exception` is a value that is thrown using the throw statement.
	      - `reason` is a value that indicates why a promise was rejected.
	      - `settled` the final resting state of a promise, fulfilled or rejected.

	      A promise can be in one of three states: pending, fulfilled, or rejected.

	      Promises that are fulfilled have a fulfillment value and are in the fulfilled
	      state.  Promises that are rejected have a rejection reason and are in the
	      rejected state.  A fulfillment value is never a thenable.

	      Promises can also be said to *resolve* a value.  If this value is also a
	      promise, then the original promise's settled state will match the value's
	      settled state.  So a promise that *resolves* a promise that rejects will
	      itself reject, and a promise that *resolves* a promise that fulfills will
	      itself fulfill.


	      Basic Usage:
	      ------------

	      ```js
	      var promise = new Promise(function(resolve, reject) {
	        // on success
	        resolve(value);

	        // on failure
	        reject(reason);
	      });

	      promise.then(function(value) {
	        // on fulfillment
	      }, function(reason) {
	        // on rejection
	      });
	      ```

	      Advanced Usage:
	      ---------------

	      Promises shine when abstracting away asynchronous interactions such as
	      `XMLHttpRequest`s.

	      ```js
	      function getJSON(url) {
	        return new Promise(function(resolve, reject){
	          var xhr = new XMLHttpRequest();

	          xhr.open('GET', url);
	          xhr.onreadystatechange = handler;
	          xhr.responseType = 'json';
	          xhr.setRequestHeader('Accept', 'application/json');
	          xhr.send();

	          function handler() {
	            if (this.readyState === this.DONE) {
	              if (this.status === 200) {
	                resolve(this.response);
	              } else {
	                reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
	              }
	            }
	          };
	        });
	      }

	      getJSON('/posts.json').then(function(json) {
	        // on fulfillment
	      }, function(reason) {
	        // on rejection
	      });
	      ```

	      Unlike callbacks, promises are great composable primitives.

	      ```js
	      Promise.all([
	        getJSON('/posts'),
	        getJSON('/comments')
	      ]).then(function(values){
	        values[0] // => postsJSON
	        values[1] // => commentsJSON

	        return values;
	      });
	      ```

	      @class Promise
	      @param {function} resolver
	      Useful for tooling.
	      @constructor
	    */
	    function lib$es6$promise$promise$$Promise(resolver) {
	      this[lib$es6$promise$$internal$$PROMISE_ID] = lib$es6$promise$$internal$$nextId();
	      this._result = this._state = undefined;
	      this._subscribers = [];

	      if (lib$es6$promise$$internal$$noop !== resolver) {
	        typeof resolver !== 'function' && lib$es6$promise$promise$$needsResolver();
	        this instanceof lib$es6$promise$promise$$Promise ? lib$es6$promise$$internal$$initializePromise(this, resolver) : lib$es6$promise$promise$$needsNew();
	      }
	    }

	    lib$es6$promise$promise$$Promise.all = lib$es6$promise$promise$all$$default;
	    lib$es6$promise$promise$$Promise.race = lib$es6$promise$promise$race$$default;
	    lib$es6$promise$promise$$Promise.resolve = lib$es6$promise$promise$resolve$$default;
	    lib$es6$promise$promise$$Promise.reject = lib$es6$promise$promise$reject$$default;
	    lib$es6$promise$promise$$Promise._setScheduler = lib$es6$promise$asap$$setScheduler;
	    lib$es6$promise$promise$$Promise._setAsap = lib$es6$promise$asap$$setAsap;
	    lib$es6$promise$promise$$Promise._asap = lib$es6$promise$asap$$asap;

	    lib$es6$promise$promise$$Promise.prototype = {
	      constructor: lib$es6$promise$promise$$Promise,

	    /**
	      The primary way of interacting with a promise is through its `then` method,
	      which registers callbacks to receive either a promise's eventual value or the
	      reason why the promise cannot be fulfilled.

	      ```js
	      findUser().then(function(user){
	        // user is available
	      }, function(reason){
	        // user is unavailable, and you are given the reason why
	      });
	      ```

	      Chaining
	      --------

	      The return value of `then` is itself a promise.  This second, 'downstream'
	      promise is resolved with the return value of the first promise's fulfillment
	      or rejection handler, or rejected if the handler throws an exception.

	      ```js
	      findUser().then(function (user) {
	        return user.name;
	      }, function (reason) {
	        return 'default name';
	      }).then(function (userName) {
	        // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
	        // will be `'default name'`
	      });

	      findUser().then(function (user) {
	        throw new Error('Found user, but still unhappy');
	      }, function (reason) {
	        throw new Error('`findUser` rejected and we're unhappy');
	      }).then(function (value) {
	        // never reached
	      }, function (reason) {
	        // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
	        // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
	      });
	      ```
	      If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.

	      ```js
	      findUser().then(function (user) {
	        throw new PedagogicalException('Upstream error');
	      }).then(function (value) {
	        // never reached
	      }).then(function (value) {
	        // never reached
	      }, function (reason) {
	        // The `PedgagocialException` is propagated all the way down to here
	      });
	      ```

	      Assimilation
	      ------------

	      Sometimes the value you want to propagate to a downstream promise can only be
	      retrieved asynchronously. This can be achieved by returning a promise in the
	      fulfillment or rejection handler. The downstream promise will then be pending
	      until the returned promise is settled. This is called *assimilation*.

	      ```js
	      findUser().then(function (user) {
	        return findCommentsByAuthor(user);
	      }).then(function (comments) {
	        // The user's comments are now available
	      });
	      ```

	      If the assimliated promise rejects, then the downstream promise will also reject.

	      ```js
	      findUser().then(function (user) {
	        return findCommentsByAuthor(user);
	      }).then(function (comments) {
	        // If `findCommentsByAuthor` fulfills, we'll have the value here
	      }, function (reason) {
	        // If `findCommentsByAuthor` rejects, we'll have the reason here
	      });
	      ```

	      Simple Example
	      --------------

	      Synchronous Example

	      ```javascript
	      var result;

	      try {
	        result = findResult();
	        // success
	      } catch(reason) {
	        // failure
	      }
	      ```

	      Errback Example

	      ```js
	      findResult(function(result, err){
	        if (err) {
	          // failure
	        } else {
	          // success
	        }
	      });
	      ```

	      Promise Example;

	      ```javascript
	      findResult().then(function(result){
	        // success
	      }, function(reason){
	        // failure
	      });
	      ```

	      Advanced Example
	      --------------

	      Synchronous Example

	      ```javascript
	      var author, books;

	      try {
	        author = findAuthor();
	        books  = findBooksByAuthor(author);
	        // success
	      } catch(reason) {
	        // failure
	      }
	      ```

	      Errback Example

	      ```js

	      function foundBooks(books) {

	      }

	      function failure(reason) {

	      }

	      findAuthor(function(author, err){
	        if (err) {
	          failure(err);
	          // failure
	        } else {
	          try {
	            findBoooksByAuthor(author, function(books, err) {
	              if (err) {
	                failure(err);
	              } else {
	                try {
	                  foundBooks(books);
	                } catch(reason) {
	                  failure(reason);
	                }
	              }
	            });
	          } catch(error) {
	            failure(err);
	          }
	          // success
	        }
	      });
	      ```

	      Promise Example;

	      ```javascript
	      findAuthor().
	        then(findBooksByAuthor).
	        then(function(books){
	          // found books
	      }).catch(function(reason){
	        // something went wrong
	      });
	      ```

	      @method then
	      @param {Function} onFulfilled
	      @param {Function} onRejected
	      Useful for tooling.
	      @return {Promise}
	    */
	      then: lib$es6$promise$then$$default,

	    /**
	      `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
	      as the catch block of a try/catch statement.

	      ```js
	      function findAuthor(){
	        throw new Error('couldn't find that author');
	      }

	      // synchronous
	      try {
	        findAuthor();
	      } catch(reason) {
	        // something went wrong
	      }

	      // async with promises
	      findAuthor().catch(function(reason){
	        // something went wrong
	      });
	      ```

	      @method catch
	      @param {Function} onRejection
	      Useful for tooling.
	      @return {Promise}
	    */
	      'catch': function(onRejection) {
	        return this.then(null, onRejection);
	      }
	    };
	    var lib$es6$promise$enumerator$$default = lib$es6$promise$enumerator$$Enumerator;
	    function lib$es6$promise$enumerator$$Enumerator(Constructor, input) {
	      this._instanceConstructor = Constructor;
	      this.promise = new Constructor(lib$es6$promise$$internal$$noop);

	      if (!this.promise[lib$es6$promise$$internal$$PROMISE_ID]) {
	        lib$es6$promise$$internal$$makePromise(this.promise);
	      }

	      if (lib$es6$promise$utils$$isArray(input)) {
	        this._input     = input;
	        this.length     = input.length;
	        this._remaining = input.length;

	        this._result = new Array(this.length);

	        if (this.length === 0) {
	          lib$es6$promise$$internal$$fulfill(this.promise, this._result);
	        } else {
	          this.length = this.length || 0;
	          this._enumerate();
	          if (this._remaining === 0) {
	            lib$es6$promise$$internal$$fulfill(this.promise, this._result);
	          }
	        }
	      } else {
	        lib$es6$promise$$internal$$reject(this.promise, lib$es6$promise$enumerator$$validationError());
	      }
	    }

	    function lib$es6$promise$enumerator$$validationError() {
	      return new Error('Array Methods must be provided an Array');
	    }

	    lib$es6$promise$enumerator$$Enumerator.prototype._enumerate = function() {
	      var length  = this.length;
	      var input   = this._input;

	      for (var i = 0; this._state === lib$es6$promise$$internal$$PENDING && i < length; i++) {
	        this._eachEntry(input[i], i);
	      }
	    };

	    lib$es6$promise$enumerator$$Enumerator.prototype._eachEntry = function(entry, i) {
	      var c = this._instanceConstructor;
	      var resolve = c.resolve;

	      if (resolve === lib$es6$promise$promise$resolve$$default) {
	        var then = lib$es6$promise$$internal$$getThen(entry);

	        if (then === lib$es6$promise$then$$default &&
	            entry._state !== lib$es6$promise$$internal$$PENDING) {
	          this._settledAt(entry._state, i, entry._result);
	        } else if (typeof then !== 'function') {
	          this._remaining--;
	          this._result[i] = entry;
	        } else if (c === lib$es6$promise$promise$$default) {
	          var promise = new c(lib$es6$promise$$internal$$noop);
	          lib$es6$promise$$internal$$handleMaybeThenable(promise, entry, then);
	          this._willSettleAt(promise, i);
	        } else {
	          this._willSettleAt(new c(function(resolve) { resolve(entry); }), i);
	        }
	      } else {
	        this._willSettleAt(resolve(entry), i);
	      }
	    };

	    lib$es6$promise$enumerator$$Enumerator.prototype._settledAt = function(state, i, value) {
	      var promise = this.promise;

	      if (promise._state === lib$es6$promise$$internal$$PENDING) {
	        this._remaining--;

	        if (state === lib$es6$promise$$internal$$REJECTED) {
	          lib$es6$promise$$internal$$reject(promise, value);
	        } else {
	          this._result[i] = value;
	        }
	      }

	      if (this._remaining === 0) {
	        lib$es6$promise$$internal$$fulfill(promise, this._result);
	      }
	    };

	    lib$es6$promise$enumerator$$Enumerator.prototype._willSettleAt = function(promise, i) {
	      var enumerator = this;

	      lib$es6$promise$$internal$$subscribe(promise, undefined, function(value) {
	        enumerator._settledAt(lib$es6$promise$$internal$$FULFILLED, i, value);
	      }, function(reason) {
	        enumerator._settledAt(lib$es6$promise$$internal$$REJECTED, i, reason);
	      });
	    };
	    function lib$es6$promise$polyfill$$polyfill() {
	      var local;

	      if (typeof global !== 'undefined') {
	          local = global;
	      } else if (typeof self !== 'undefined') {
	          local = self;
	      } else {
	          try {
	              local = Function('return this')();
	          } catch (e) {
	              throw new Error('polyfill failed because global object is unavailable in this environment');
	          }
	      }

	      var P = local.Promise;

	      if (P && Object.prototype.toString.call(P.resolve()) === '[object Promise]' && !P.cast) {
	        return;
	      }

	      local.Promise = lib$es6$promise$promise$$default;
	    }
	    var lib$es6$promise$polyfill$$default = lib$es6$promise$polyfill$$polyfill;

	    var lib$es6$promise$umd$$ES6Promise = {
	      'Promise': lib$es6$promise$promise$$default,
	      'polyfill': lib$es6$promise$polyfill$$default
	    };

	    /* global define:true module:true window: true */
	    if ("function" === 'function' && __webpack_require__(5)['amd']) {
	      !(__WEBPACK_AMD_DEFINE_RESULT__ = function() { return lib$es6$promise$umd$$ES6Promise; }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if (typeof module !== 'undefined' && module['exports']) {
	      module['exports'] = lib$es6$promise$umd$$ES6Promise;
	    } else if (typeof this !== 'undefined') {
	      this['ES6Promise'] = lib$es6$promise$umd$$ES6Promise;
	    }

	    lib$es6$promise$polyfill$$default();
	}).call(this);


	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2), (function() { return this; }()), __webpack_require__(3)(module)))

/***/ },
/* 2 */
/***/ function(module, exports) {

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 4 */
/***/ function(module, exports) {

	/* (ignored) */

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = function() { throw new Error("define cannot be used indirect"); };


/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';



	function createCanvas(canvasId) {
		
		canvasId = canvasId || 'gameCanvas';	
		var canvas = document.createElement('canvas');
		canvas.id     = canvasId;
		document.body.appendChild(canvas);

		return canvas;
	}

	function init() {

		var canvas = createCanvas('gameCanvas');	
		var gameEngine = new BABYLON.Engine(canvas, true);
		gameEngine.enableOfflineSupport = true;


		//var controlPad = new initControl();
			
		window.addEventListener("resize", function () {
			gameEngine.resize(); // Watch for browser/canvas resize events
		});


		document.body.addEventListener("resize", function () {
			gameEngine.resize(); // Watch for browser/canvas resize events
		});

		var GAME = {
			// controlPad : controlPad,
			scene : null,
			camera : null,
			canvas : canvas,
			engine : gameEngine,

			state : {

				states : { },
				
				add : function (stateName, hooks) {
					
					this.states[stateName] = hooks;
					return this;
				},

				start : function (stateName) {

					var stateExists = stateName in this.states;

					if (!stateExists) {
						//TODO add error
						return this;
					}

					var me = GAME;
					var sceneHooks = this.states[stateName];

					GAME.scene = new BABYLON.Scene(gameEngine, true);
					GAME.scene.collisionsEnabled = true;


					GAME.camera = new BABYLON.FreeCamera('player', new BABYLON.Vector3(0, 0, -700), GAME.scene);

					GAME.scene.activeCamera = GAME.camera;


					// console.log(GAME.camera.inputs.attached.mouse.detach());



					GAME.camera.ellipsoid = new BABYLON.Vector3(10, 10, 10);
			    	GAME.camera.checkCollisions = true;



					// GAME.scene.enablePhysics = true;
					// GAME.camera.inputs.attached.xcraftStick.detachControl();


					var next = function () {

						try {
						
							'create' in sceneHooks && sceneHooks.create.apply(GAME);

							gameEngine.runRenderLoop(function () {
								'update' in sceneHooks && sceneHooks.update.apply(GAME);
								'render' in sceneHooks ? sceneHooks.render.apply(GAME) : me.scene.render();						
							});

						} catch (e) {
							console.log(e.stack);
						}
					}


					try {
						'preload' in sceneHooks ? sceneHooks.preload.apply(GAME, [next]) : next();
					} catch (e) {
						console.log(e.stack);
					}
					


					return this;
				}
			}
		}

		return GAME;
	}



	module.exports['createCanvas'] = createCanvas;
	module.exports['init'] = init;




/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';


	var planetBoundingBox;
	var FreeCameraXCraftControlInput = __webpack_require__(8);


	module.exports = function makePlanetScene(planetName) {
		
		var planetInfoUrl = "src/planets/" + planetName + '/';

		return {

			preload : function (cb) {

				var me = this;


				//this.scene.debugLayer.show();

				// var camSensor = new BABYLON.Mesh.CreateBox("sensor", 1, me.scene);
				// camSensor.material = new BABYLON.StandardMaterial("camMat", me.scene);
				// camSensor.isVisible = true;
				// camSensor.material.wireframe = true;
				// camSensor.scaling = new BABYLON.Vector3(.01, .01, .01);
				// camSensor.position = new BABYLON.Vector3(0, .005, 0);
				// camSensor.parent = me.camera;





				// var testMesh = new BABYLON.Mesh.CreateBox("testMesh", 40, me.scene);
				// testMesh.position = BABYLON.Vector3.Zero();
				// //currentPlanetGroup.isVisible = false;

				// currentPlanetGroup = new BABYLON.Mesh.CreateBox("currentPlanet", 1200, me.scene);
				// currentPlanetGroup.position = BABYLON.Vector3.Zero();
				// currentPlanetGroup.isVisible = false;
				//currentPlanetGroup.isPickable = true;

				// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
				// console.log('Collider radius', currentPlanetGroup._collider.radius);


				// console.log('Meshes', this.scene.meshes);








				// var halfSize = 300;

				// currentPlanetGroup.showBoundingBox = true;
				// currentPlanetGroup.checkCollisions = true; 

				//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));
				//console.log(currentPlanetGroup.getBoundingInfo());
				//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));


				//currentPlanetGroup.getScene().collisionCoordinator.onMeshUpdated(currentPlanetGroup);

				//currentPlanetGroup._collider
				
				//console.log();

				// currentPlanetGroup.getScene().collisionCoordinator.getNewPosition(
	   //            	currentPlanetGroup._oldPositionForCollisions,
	   //            	new BABYLON.Vector3(0, 0, 0),
	   //            	currentPlanetGroup._collider,
	   //            	3,
	   //            	currentPlanetGroup,
	   //            	currentPlanetGroup._onCollisionPositionChange,
	   //            	currentPlanetGroup.uniqueId
	   //        	);

				// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
				// console.log('Collider radius', currentPlanetGroup._collider);


				
				// console.log(currentPlanetGroup.getBoundingInfo());






				// var newPlanetGroup = new BABYLON.Mesh.CreateBox("currentPlanet2", 1350, me.scene);
				// newPlanetGroup.position = BABYLON.Vector3.Zero();

				// console.log('Elsipsoid', newPlanetGroup.ellipsoid);
				// console.log('Collider radius', newPlanetGroup._collider);

				// var halfSize = 300;

				// newPlanetGroup.showBoundingBox = true;
				// newPlanetGroup.checkCollisions = true; 


				// this.scene.meshes.forEach(function (mesh) {
				// 	console.log(mesh.name);
				// })

				//currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));
				//console.log(currentPlanetGroup.getBoundingInfo());
				// currentPlanetGroup._boundingInfo = new BABYLON.BoundingInfo(new BABYLON.Vector3(-halfSize, -halfSize, -halfSize), new BABYLON.Vector3(halfSize, halfSize, halfSize));

				// console.log('Elsipsoid', currentPlanetGroup.ellipsoid);
				// console.log('Collider radius', currentPlanetGroup._collider.radius);


				
				// console.log(currentPlanetGroup.getBoundingInfo());







				//currentPlanetGroup.getBoundingInfo().update(currentPlanetGroup.worldMatrixFromCache);
				// currentPlanetGroup.computeWorldMatrix(true);
				// this.camera.computeWorldMatrix(true);
				//currentPlanetGroup.getBoundingInfo().update();

				//currentPlanetGroup.refreshBoundingInfo()
	 
				//currentPlanetGroup._boundingInfo.update();



				// currentPlanetGroup.onCollide = function (mesh) {
				// 	//console.log(mesh);
				// 	console.log('```````````````````````````````````````Mesh collide````````````````````````````````````````');

				// }

				// this.camera.onCollide = function () {
				// 	console.log('```````````````````````````````````````Camera collide````````````````````````````````````````');
				// }	


				// var matBB = new BABYLON.StandardMaterial("matBB", me.scene);
				// matBB.emissiveColor = new BABYLON.Color3(1, 1, 1);
				// matBB.wireframe = true;

				// currentPlanetGroup.material = matBB;



				BABYLON.SceneLoader.ImportMesh('',  planetInfoUrl, "object.json", this.scene, function loadSuccess (newMeshes) {

					// console.log('Meshes was loaded');

					planetBoundingBox = new BABYLON.Mesh.CreateSphere("planetBoundingBox", 10, 320, me.scene);
					

					planetBoundingBox.position = BABYLON.Vector3.Zero();
					planetBoundingBox.checkCollisions = true;
					planetBoundingBox.isVisible = false;



					// var vls = new BABYLON.VolumetricLightScatteringPostProcess('vls', { postProcessRatio: 1.0, passRatio: 0.5 }, me.camera, planetBoundingBox, 75, BABYLON.Texture.BILINEAR_SAMPLINGMODE, me.engine, false);



					var wireframeMaterial = new BABYLON.StandardMaterial("wireframe", me.scene);   
					wireframeMaterial.wireframe = true;

					planetBoundingBox.material = wireframeMaterial;

					newMeshes.forEach(function (mesh) {
						mesh.parent = planetBoundingBox;
						me.scene.stopAnimation(mesh);
					});

					planetBoundingBox.position = BABYLON.Vector3.Zero();	

					cb();


				});
				
			},

			create : function () {

				var me = this;
				
				me.scene.clearColor = 'black';
				me.scene.collisionsEnabled = true;


				var skybox = BABYLON.Mesh.CreateBox("skyBox", 5000.0, me.scene);



				var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
				skyboxMaterial.backFaceCulling = false;
				skyboxMaterial.disableLighting = true;
				skybox.infiniteDistance = true;

				
				skybox.material = skyboxMaterial;
				skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
				skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
				skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(planetInfoUrl + "skybox/1", this.scene);
				skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;




				me.camera.inputs.add(new FreeCameraXCraftControlInput());	
				me.camera.inputs.removeByType(me.camera.inputs.attached.keyboard.getTypeName());
				me.camera.attachControl(me.canvas, false);



				// console.log(me.camera.fov);

				// me.camera.fov = 0.1

				me.camera.position = new BABYLON.Vector3(0,  100, -200);
				me.camera.cameraDirection = new BABYLON.Vector3(0, 0, 0);
				me.camera.setTarget(new BABYLON.Vector3(-100, 100, 0));


				// me.camera.cameraRotation = new BABYLON.Vector3(0, -1);


			    
				var planetMesh = me.scene.getMeshByName("planetBoundingBox");

				planetMesh.scaling.x = 1.3;
				planetMesh.scaling.y = 1.3;
				planetMesh.scaling.z = 1.3;


				// planetMesh.scale.y = 2;


			    /* Motion blur
				BABYLON.Effect.ShadersStore["bwPixelShader"] =
				"precision highp float;" +
				"varying vec2 vUV;" +
				"uniform sampler2D textureSampler;" +
				"uniform sampler2D scene0Sampler;" +
				"uniform sampler2D scene1Sampler;" +
				"void main(void)" + 
				"{" +
				"	gl_FragColor = texture2D(textureSampler, vUV) * 0.5 + texture2D(scene0Sampler, vUV) * 0.25 + texture2D(scene1Sampler, vUV) * 0.25;" +
				"}";


				var copies = [];
				copies.push(new BABYLON.PassPostProcess("Scene copy#1", 1.0, me.camera));
				copies.push(new BABYLON.PassPostProcess("Scene copy#2", 1.0, me.camera));
				copies.push(new BABYLON.PassPostProcess("Scene copy#3", 1.0, me.camera));
				var current = 1;
				
				me.camera.detachPostProcess(copies[1]);	
				me.camera.detachPostProcess(copies[2]);

				var postProcess = new BABYLON.PostProcess("BW", "bw", ["time"], ["scene0Sampler", "scene1Sampler"], 1.0, me.camera, BABYLON.Texture.BILINEAR_SAMPLINGMODE); 
				    postProcess.onApply = function (effect) { 
						var next = current + 1;
						
						if (next === copies.length) {
							next = 0;
						}
						effect.setTextureFromPostProcess("scene0Sampler", copies[current]);
						effect.setTextureFromPostProcess("scene1Sampler", copies[next]);						
				    }; 
				*/ 




			    this.scene.registerBeforeRender(function () {

			    	/* Motion blur
			    	var prev = current - 1;
			
					if (prev < 0) {
						prev = copies.length - 1;
					}
					
					me.camera.detachPostProcess(copies[prev]);
					
					current++;
					
					if (current == copies.length) {
						current = 0;
					}
					
					var prev = current - 1;
					
					if (prev < 0) {
						prev = copies.length - 1;
					}
					
					me.camera.attachPostProcess(copies[prev], 0);
					*/






					// me.scene.getMeshByName("planetBoundingBox").rotation.y += 0.05; 

			    	
			    	if (planetMesh) {
			    		planetMesh.rotation.y += .0003; 
			    	}


			    	// var skyBoxMesh = me.scene.getMeshByName("skyBox");
			    	// if (planetMesh) {
			    	// 	skyBoxMesh.rotation.y += .001; 
			    	// }

			    });

			    new BABYLON.HemisphericLight('mainLight', new BABYLON.Vector3(0, 1, 0), me.scene);

			},

			update : function () {

			},

			// render : function () {
				
			// }
		}
	};

/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';


	function initControl(options) {

	    var controlPad = document.createElement('div');
	    controlPad.id = 'control';
	    document.body.appendChild(controlPad);

	    var padHandleStyle = null;
	    var controlPadSize = 154;
	    var padHandleSize = 86;
	    var controlActive = false;
	    var mouseGlobalX, mouseGLobalY;


	    var defaultOptions = {
	        keys : {
	            up : 87,
	            left : 65,
	            down : 83,
	            right : 68
	        }
	    }

	    options = options || defaultOptions; //TODO add options merge


	    var keyboardKeys = {
	        up : [38],
	        down : [40],
	        left : [37],
	        right : [39]
	    }


	    if (options instanceof Object && 'keys' in options && options.keys instanceof Object) {

	        Object.keys(keyboardKeys).forEach(function (keyIdx) {
	            
	            var keyCodeExists = keyIdx in options.keys;

	            if (!keyCodeExists) {
	                return;
	            }

	            var currentKeysFromOptions = options.keys[keyIdx];
	            currentKeysFromOptions = typeof currentKeysFromOptions === 'string'   ? parseInt(currentKeysFromOptions) : currentKeysFromOptions;
	            var isAllowedArray = currentKeysFromOptions instanceof Array, isAllowedNUmber = typeof currentKeysFromOptions === 'number';

	            if (!isAllowedArray) {
	                currentKeysFromOptions = [currentKeysFromOptions];
	            }

	            currentKeysFromOptions
	                .forEach(function (keyCode) {
	                    keyCode = parseInt(keyCode);
	                    if (!~keyboardKeys[keyIdx].indexOf(keyCode)) keyboardKeys[keyIdx].push(keyCode);                    
	                });
	            ;      

	        });
	    } 

	    // 154 x 154 - field
	    // 86 x 86 - pad


	    function getElementPosition(element) {
	        var boundRectOfPad = element.getBoundingClientRect();
	        return { x: boundRectOfPad.left + window.scrollX, y: boundRectOfPad.top + window.scrollY }
	    }

	    
	    function convertPositionToPadRelated(controlPadRelatedPosition) {
	        
	        var resultPosition = {
	            x : 0, y : 0
	        };

	        resultPosition.x = controlPadRelatedPosition.x > 0 ? 0 : Math.abs(controlPadRelatedPosition.x);
	        resultPosition.y = controlPadRelatedPosition.y > 0 ? 0 : Math.abs(controlPadRelatedPosition.y);

	        resultPosition.x = resultPosition.x > controlPadSize ? controlPadSize : resultPosition.x;
	        resultPosition.y = resultPosition.y > controlPadSize ? controlPadSize : resultPosition.y;

	        return resultPosition;
	    }

	    
	    function getOffsetRect(offset) {    
	        
	        return {            
	            maxX : offset / 2 + padHandleSize,
	            maxY : offset / 2 + padHandleSize,
	            minX : offset,
	            minY : offset,
	        }
	    }

	    var leftRight = 0;
	    var topBottom = 0;

	    function getPadHandleCoords(relatedOnControlPosition) {
	        
	        var offset = padHandleSize / 2, resultCoords = { x: 0, y : 0 };

	        resultCoords.x = relatedOnControlPosition.x - offset;
	        resultCoords.y = relatedOnControlPosition.y - offset;

	        var offsetRect = getOffsetRect(16);
	        
	        resultCoords.x = resultCoords.x < offsetRect.minX ? offsetRect.minX : resultCoords.x;
	        resultCoords.x = resultCoords.x > offsetRect.maxX ? offsetRect.maxX : resultCoords.x;

	        resultCoords.y = resultCoords.y < offsetRect.minY ? offsetRect.minY : resultCoords.y;
	        resultCoords.y = resultCoords.y > offsetRect.maxY ? offsetRect.maxY : resultCoords.y;

	        resultCoords.x = resultCoords.x > controlPadSize ? controlPadSize : resultCoords.x;
	        resultCoords.y = resultCoords.y > controlPadSize ? controlPadSize : resultCoords.y;

	        var rectHalf = 78 / 2, rectHalfMin = rectHalf - 10, rectHalfMax = rectHalf + 10;

	        var correctX = resultCoords.x - 16;

	        if (correctX < rectHalfMin) {
	            leftRight = -1;
	        } else if (correctX > rectHalfMax) {
	            leftRight = 1;
	        }

	        var correctY = resultCoords.y - 16;

	        if (correctY < rectHalfMin) {
	            topBottom = 1;
	        } else if (correctY > rectHalfMax) {
	            topBottom = -1;
	        }



	        return resultCoords;
	    }


	    function setPadHandlePosition(mouseX, mouseY) {     
	        
	        var position = getElementPosition(controlPad);
	        var relatedPosition = {
	            x : position.x - mouseX,
	            y : position.y - mouseY,
	        }

	        var convertedPosition = convertPositionToPadRelated(relatedPosition);
	        controlPad.movePadTo(convertedPosition);
	    }   



	    controlPad.movePadTo = function (handleCoordsOnPad) {
	        
	        if (!padHandleStyle) {

	            var styleELement = document.createElement('style');
	            document.head.appendChild(styleELement);

	            var styleSheet = styleELement.sheet;
	            var str = ''
	            styleSheet.insertRule('#control::before{ ' + str + ' }', styleSheet.cssRules.length);
	            
	            padHandleStyle = styleSheet.cssRules[styleSheet.cssRules.length - 1];
	        }

	        var padHandleCoords = getPadHandleCoords(handleCoordsOnPad);
	        padHandleStyle.style.backgroundPosition = padHandleCoords.x + 'px ' + padHandleCoords.y + 'px';
	    }



	    controlPad.movePadToCenter = function () {
	        
	        if (!padHandleStyle) {

	            var styleELement = document.createElement('style');
	            document.head.appendChild(styleELement);

	            var styleSheet = styleELement.sheet;
	            var str = ''
	            styleSheet.insertRule('#control::before{ ' + str + ' }', styleSheet.cssRules.length);
	            
	            padHandleStyle = styleSheet.cssRules[styleSheet.cssRules.length - 1];
	        }

	        padHandleStyle.style.backgroundPosition = 'center center';
	    }



	    var intervalChecker = {
	        
	        checkIntervalID : null,
	        
	        start : function (onTimerCallback) {
	            
	            var me  = this;

	            if (me.checkIntervalID) {
	                this.stop();
	            } 

	            onTimerCallback();          

	            me.checkIntervalID = setInterval(function () {
	                if (onTimerCallback) {
	                    onTimerCallback();
	                }
	            }, 10);
	        },

	        stop : function () {

	            var me  = this;            
	            if (!me.checkIntervalID) {
	                return ;
	            }

	            clearInterval(me.checkIntervalID)
	        }
	    }


	    

	    var moveMatrix = {

	        stop : function () {
	            this.x = 0;
	            this.y = 0;
	            this.z = 0;
	        }

	    }, _moveMatrix = {
	        x : 0, y : 0, z: 0,
	        isStoped : function () {
	            return this.x == 0 && this.y == 0 && this.z ==0;
	        },
	        startMove : function () {
	            
	            if (this.isStoped()) {
	                var PadEvent = new CustomEvent('actorstartmove', {  });
	                controlPad.dispatchEvent(PadEvent);
	            }
	        },

	        stopMove : function () {
	            
	            if (this.isStoped()) {
	                var PadEvent = new CustomEvent('actorstopmove', {  });
	                controlPad.dispatchEvent(PadEvent);
	            }

	        },

	        moveTo : function (axis, velocity) {
	            this.startMove();            
	            this[axis] = velocity;
	            this.stopMove();        
	        }
	    };


	    Object.defineProperty(moveMatrix, 'x', {
	        configurable: false,
	        enumerable: true,        
	        get : function () {
	            return _moveMatrix['x'];
	        },        
	        set : function (value) {
	            _moveMatrix.moveTo('x', parseInt(value));
	        }
	    });

	    Object.defineProperty(moveMatrix, 'isStoped', {
	        configurable: false,
	        enumerable: true,        
	        get : function () {
	            return _moveMatrix.isStoped();
	        }        
	        // set : function (value) {
	        //     _moveMatrix.moveTo('x', parseInt(value));
	        // }
	    });


	    Object.defineProperty(moveMatrix, 'y', {
	        configurable: false,
	        enumerable: true,        
	        get : function () {
	            return _moveMatrix['y'];
	        },        
	        set : function (value) {
	            _moveMatrix.moveTo('y', parseInt(value));
	        }
	    });

	    
	    Object.defineProperty(moveMatrix, 'z', {
	        configurable: false,
	        enumerable: true,        
	        get : function () {
	            return _moveMatrix['z'];
	        },        
	        set : function (value) {
	            _moveMatrix.moveTo('z', parseInt(value));
	        }
	    });


	    intervalChecker.start(function () {
	        var PadEvent = new CustomEvent('actormove', { detail : moveMatrix });
	        controlPad.dispatchEvent(PadEvent);
	    })



	    var keyWatcher = {
	        keys : [],
	       
	        press : function (keyName) {
	            if (this.has(keyName)) return;
	            this.keys.push(keyName);
	        },

	        has : function (keyName) {
	            return ~this.keys.indexOf(keyName);
	        },

	        unpress : function (keyName) {
	            if (!this.has(keyName)) return;
	            this.keys.splice(this.keys.indexOf(keyName), 1);            
	        }
	    }

	    
	    var behaviors = {
	        
	        start : function (event) {
	            controlActive = true;
	            moveMatrix.z = 1;
	        },

	        move : function (event) {
	            

	            if (controlActive) {
	                setPadHandlePosition(event.pageX || e.originalEvent.touches[0].pageX, event.pageY || e.originalEvent.touches[0].pageY);
	                moveMatrix.x = 1 * leftRight;
	                moveMatrix.y = 1 * topBottom;
	                moveMatrix.z = 1;
	            }
	        },

	        stop : function (event) {
	            
	            controlActive = false;
	            controlPad.movePadToCenter();

	            moveMatrix.stop();

	            // var PadEvent = new CustomEvent('actorstopmove', { });
	            // controlPad.dispatchEvent(PadEvent);

	            // intervalChecker.stop();
	        },

	        keyboardDispatcher : function (event) {

	            if (~keyboardKeys.up.indexOf(event.keyCode)) {

	                if (event.type == 'keydown') {  
	                    keyWatcher.press('forward');                    
	                } 

	                if (event.type == 'keyup') {
	                    keyWatcher.unpress('forward');
	                }
	            }

	            else if (~keyboardKeys.left.indexOf(event.keyCode)) {

	                if (event.type == 'keydown') {
	                    keyWatcher.press('left');                    
	                }

	                if (event.type == 'keyup') {
	                    keyWatcher.unpress('left');
	                }
	            }

	            else if (~keyboardKeys.right.indexOf(event.keyCode)) {
	                
	                if (event.type == 'keydown') {
	                    keyWatcher.press('right');                    
	                }

	                if (event.type == 'keyup') {
	                    keyWatcher.unpress('right');
	                }
	            }

	            else if (~keyboardKeys.down.indexOf(event.keyCode)) {
	                
	                if (event.type == 'keydown') {
	                    keyWatcher.press('backward');                    
	                }

	                if (event.type == 'keyup') {
	                    keyWatcher.unpress('backward');
	                }
	            }   else {
	                return ;
	            }



	            if (!keyWatcher.keys.length) {

	                console.log();
	                return moveMatrix.stop();
	            }

	            var isSideMove = keyWatcher.has('left') || keyWatcher.has('right');
	            var isDirectMove = keyWatcher.has('forward') || keyWatcher.has('backward');

	            if (isDirectMove && !isSideMove) {
	                // console.log('isDIrect move');
	                moveMatrix.z = keyWatcher.has('forward') && keyWatcher.has('backward')  ? 0 :
	                    keyWatcher.has('forward') ?  1 : -1;

	                moveMatrix.x = 0;

	                return ;
	            }   

	            if (isSideMove) {

	                moveMatrix.x = keyWatcher.has('left') && keyWatcher.has('right')  ? 0 :
	                    keyWatcher.has('left') ?  -1 : 1;

	                var moveIsForward = keyWatcher.has('forward') || 
	                    !keyWatcher.has('backward') ||
	                    (keyWatcher.has('forward') && keyWatcher.has('backward'));                

	                moveMatrix.z =  moveIsForward ? 1 : -1;

	            } 
	            
	            //return moveMatrix.stop();            

	        }
	    }

	    controlPad.addEventListener('mousedown', behaviors.start, false);   
	    controlPad.addEventListener('touchstart', behaviors.start, false);
	    document.addEventListener('mousemove', behaviors.move, false);
	    document.addEventListener('touchmove', behaviors.move, false);  
	    document.addEventListener('mouseup', behaviors.stop, false);
	    document.addEventListener('touchend', behaviors.stop, false);       
	   
	    document.addEventListener('keydown', behaviors.keyboardDispatcher, false);
	    document.addEventListener('keyup', behaviors.keyboardDispatcher, false);  
	    document.addEventListener('blur', behaviors.keyboardDispatcher, false);


	    return {
	        pad : controlPad,
	        detach : function () {    

	            controlPad.removeEventListener('mousedown', behaviors.start, false);   
	            controlPad.removeEventListener('touchstart', behaviors.start, false);
	            document.removeEventListener('mousemove', behaviors.move, false);
	            document.removeEventListener('touchmove', behaviors.move, false);  
	            document.removeEventListener('mouseup', behaviors.stop, false);
	            document.removeEventListener('touchend', behaviors.stop, false);              

	            document.removeEventListener('keydown', behaviors.keyboardDispatcher, false);
	            document.removeEventListener('keyup', behaviors.keyboardDispatcher, false);  
	            document.removeEventListener('blur', behaviors.keyboardDispatcher, false);

	            controlPad.parentNode.removeChild(controlPad);
	            intervalChecker.stop();

	        }
	    };
	}

	function FreeCameraXCraftControlInput(stickSpeed, angularSensibility) {

	    this.angularSensibility = angularSensibility || 1000;
	    this._stickSpeed = stickSpeed || 0.7;
	    this._controlPad = null;
	    this._isMoved = false;
	    // this._moves = [];

	    this._currentMove = null;
	};

	FreeCameraXCraftControlInput.prototype.detachControl = function (element) {

	    if (this._controlPad) {
	        this._controlPad.detach();
	        delete this._controlPad;
	        this._controlPad = null;
	    }
	};


	FreeCameraXCraftControlInput.prototype.attachControl = function (element, noPreventDefault) {

	    var me = this;

	    this._controlPad = initControl();
	    this._controlPad.pad.addEventListener('actorstartmove', function (event) {

	    }, false);

	    this._controlPad.pad.addEventListener('actorstopmove', function (event) {        
	        me._currentMove = null;
	    }, false);

	    this._controlPad.pad.addEventListener('actormove', function (event) {
	        me._currentMove = event.detail;
	    }, false);

	};


	FreeCameraXCraftControlInput.prototype.getTypeName = function (element) {
	    return "FreeCameraXCraftControlInput";
	};


	FreeCameraXCraftControlInput.prototype.getSimpleName = function (element) {
	    return "xcraftStick";
	};

	FreeCameraXCraftControlInput.prototype.checkInputs = function () {

	    var camera = this.camera;
	    if (this._currentMove && !this._currentMove.isStoped) {

	        var speed = camera._computeLocalCameraSpeed();
	        camera._localDirection.copyFromFloats(
	            this._stickSpeed * speed * this._currentMove.x,
	            0, // this._stickSpeed *  speed * this._currentMove.y,
	            this._stickSpeed *  speed * this._currentMove.z
	        );

	        camera.getViewMatrix().invertToRef(camera._cameraTransformMatrix);

	        //this.angularSensibility = 100;

	        if (this._currentMove.y !== 0) {
	            camera.cameraRotation.x -= this._currentMove.y / this.angularSensibility;
	        }

	        if (this._currentMove.x !== 0) {
	            camera.cameraRotation.y += this._currentMove.x / this.angularSensibility;
	        }

	        BABYLON.Vector3.TransformNormalToRef(camera._localDirection, camera._cameraTransformMatrix, camera._transformedDirection);
	        camera.cameraDirection.addInPlace(camera._transformedDirection);
	    }

	}

	module.exports = FreeCameraXCraftControlInput;




/***/ }
/******/ ]);